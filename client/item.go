package client

import (
	"fmt"
	"gitlab.com/maivn/avitogo/domain"
	"net/http"
	"net/url"
)

const (
	getItemResource = "/core/v1/accounts/%d/items/%d"
)

type Item struct {
	HttpClient *HttpClient
}

func newItem(httpClient *HttpClient) Item {
	return Item{HttpClient: httpClient}
}

func (i *Item) GetItem(userId, itemId int64) (*domain.ResponseGetItem, error) {
	option := OptionsRequest{
		Method: http.MethodGet,
		Url: url.URL{
			Scheme: "https",
			Host:   "api.avito.ru",
			Path:   fmt.Sprintf(getItemResource, userId, itemId),
		},
	}

	var resp domain.ResponseGetItem
	err := i.HttpClient.request(&option, &resp)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}
