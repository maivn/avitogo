package client

import (
	"gitlab.com/maivn/avitogo/domain"
	"net/http"
	"net/url"
	"time"
)

const (
	getTokenResource = "/token"
	oauthResource    = "/oauth"
)

type Token struct {
	HttpClient *HttpClient
}

func newToken(httpClient *HttpClient) Token {
	return Token{HttpClient: httpClient}
}

func (t *Token) GetByClientIdAndSecret(clientId, clientSecret string) (*domain.Token, error) {
	option := OptionsRequest{
		Method: http.MethodPost,
		Url: url.URL{
			Scheme: "https",
			Host:   "api.avito.ru",
			Path:   getTokenResource,
			RawQuery: url.Values{
				"client_id":     []string{clientId},
				"client_secret": []string{clientSecret},
				"grant_type":    []string{"client_credentials"},
			}.Encode(),
		},
		NotRefresh: true,
	}

	now := time.Now()

	var token domain.Token
	err := t.HttpClient.request(&option, &token)
	if err != nil {
		return nil, err
	}

	token.ExpiresAt = now.Add(time.Duration(token.ExpiresIn) * time.Second)
	t.HttpClient.Token = &token

	return &token, nil
}

func (t *Token) GetByCode(code string) (*domain.Token, error) {
	option := OptionsRequest{
		Method: http.MethodPost,
		Url: url.URL{
			Scheme: "https",
			Host:   "api.avito.ru",
			Path:   getTokenResource,
			RawQuery: url.Values{
				"client_id":     []string{t.HttpClient.App.ClientId},
				"client_secret": []string{t.HttpClient.App.ClientSecret},
				"code":          []string{code},
				"grant_type":    []string{"authorization_code"},
			}.Encode(),
		},
		NotRefresh: true,
	}

	now := time.Now()

	var token domain.Token
	err := t.HttpClient.request(&option, &token)
	if err != nil {
		return nil, err
	}

	token.ExpiresAt = now.Add(time.Duration(token.ExpiresIn) * time.Second)
	t.HttpClient.Token = &token

	return &token, nil
}

func (t *Token) SetCallback(callback func(*domain.Token) error) {
	t.HttpClient.Callback = callback
}
