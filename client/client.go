package client

import (
	"gitlab.com/maivn/avitogo/domain"
)

type Avito struct {
	Token   Token   `json:"token"`
	Message Message `json:"message"`
	User    User    `json:"user"`
	Item    Item    `json:"item"`
}

func NewAvito(token *domain.Token, app *domain.App) *Avito {
	//RegisterDecoderFunc()

	/*if !strings.Contains(subdomain, ".amocrm.ru") {
		subdomain = subdomain + ".amocrm.ru"
	}*/

	if token == nil {
		token = &domain.Token{}
	}

	if app == nil {
		app = &domain.App{}
	}

	amoHttpClient := newHttpClient(token, app)
	return &Avito{
		Token:   newToken(amoHttpClient),
		Message: newMessage(amoHttpClient),
		User:    newUser(amoHttpClient),
		Item:    newItem(amoHttpClient),
	}
}
