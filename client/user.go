package client

import (
	"gitlab.com/maivn/avitogo/domain"
	"net/http"
	"net/url"
)

const (
	getUserInfoSelfResource = "/core/v1/accounts/self"
)

type User struct {
	HttpClient *HttpClient
}

func newUser(httpClient *HttpClient) User {
	return User{HttpClient: httpClient}
}

func (u *User) GetUserInfoSelf() (*domain.ResponseGetUserInfoSelf, error) {
	option := OptionsRequest{
		Method: http.MethodGet,
		Url: url.URL{
			Scheme: "https",
			Host:   "api.avito.ru",
			Path:   getUserInfoSelfResource,
		},
	}

	var resp domain.ResponseGetUserInfoSelf
	err := u.HttpClient.request(&option, &resp)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}
