package client

import (
	"fmt"
	"gitlab.com/maivn/avitogo/domain"
	"net/http"
	"net/url"
)

const (
	sendMessageResource        = "/messenger/v1/accounts/%d/chats/%s/messages"
	getSubscriptionsResource   = "/messenger/v1/subscriptions"
	getChatByIdResource        = "/messenger/v2/accounts/%d/chats/%s"
	getMessagesResource        = "/messenger/v3/accounts/%d/chats/%s/messages"
	webhookSubscribeResource   = "/messenger/v3/webhook"
	webhookUnsubscribeResource = "/messenger/v1/webhook/unsubscribe"
)

type Message struct {
	HttpClient *HttpClient
}

func newMessage(httpClient *HttpClient) Message {
	return Message{HttpClient: httpClient}
}

func (m *Message) SendMessage(userId int64, chatId string, message domain.SendMessage) (*domain.ResponseSendMessage, error) {
	option := OptionsRequest{
		Method: http.MethodPost,
		Url: url.URL{
			Scheme: "https",
			Host:   "api.avito.ru",
			Path:   fmt.Sprintf(sendMessageResource, userId, chatId),
		},
		Body: message,
	}

	var resp domain.ResponseSendMessage
	err := m.HttpClient.request(&option, &resp)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (m *Message) GetSubscriptions() (*domain.ResponseGetSubscriptions, error) {
	option := OptionsRequest{
		Method: http.MethodPost,
		Url: url.URL{
			Scheme: "https",
			Host:   "api.avito.ru",
			Path:   getSubscriptionsResource,
		},
	}

	var resp domain.ResponseGetSubscriptions
	err := m.HttpClient.request(&option, &resp)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (m *Message) GetChatById(userId int64, chatId string) (*domain.ResponseGetChatById, error) {
	option := OptionsRequest{
		Method: http.MethodGet,
		Url: url.URL{
			Scheme: "https",
			Host:   "api.avito.ru",
			Path:   fmt.Sprintf(getChatByIdResource, userId, chatId),
		},
	}

	var resp domain.ResponseGetChatById
	err := m.HttpClient.request(&option, &resp)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (m *Message) GetMessages(userId int64, chatId string, values url.Values) (*domain.ResponseGetMessages, error) {
	option := OptionsRequest{
		Method: http.MethodGet,
		Url: url.URL{
			Scheme:   "https",
			Host:     "api.avito.ru",
			Path:     fmt.Sprintf(getMessagesResource, userId, chatId),
			RawQuery: values.Encode(),
		},
	}

	var resp domain.ResponseGetMessages
	err := m.HttpClient.request(&option, &resp)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (m *Message) WebhookSubscribe(webhookSubscribe domain.WebhookSubscribe) (*domain.ResponseWebhookSubscribe, error) {
	option := OptionsRequest{
		Method: http.MethodPost,
		Url: url.URL{
			Scheme: "https",
			Host:   "api.avito.ru",
			Path:   webhookSubscribeResource,
		},
		Body: webhookSubscribe,
	}

	var resp domain.ResponseWebhookSubscribe
	err := m.HttpClient.request(&option, &resp)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (m *Message) WebhookUnsubscribe(webhookUnsubscribe domain.WebhookUnsubscribe) (*domain.ResponseWebhookUnsubscribe, error) {
	option := OptionsRequest{
		Method: http.MethodPost,
		Url: url.URL{
			Scheme: "https",
			Host:   "api.avito.ru",
			Path:   webhookUnsubscribeResource,
		},
		Body: webhookUnsubscribe,
	}

	var resp domain.ResponseWebhookUnsubscribe
	err := m.HttpClient.request(&option, &resp)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}
