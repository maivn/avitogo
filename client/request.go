package client

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/maivn/avitogo/domain"
	"io"
	"net/http"
	"net/url"
	"time"
)

const refreshTokenResource = "/token"

type HttpClient struct {
	Token *domain.Token
	App   *domain.App

	Callback func(*domain.Token) error `json:"-"`
}

func newHttpClient(token *domain.Token, app *domain.App) *HttpClient {
	return &HttpClient{
		Token: token,
		App:   app,
	}
}

type OptionsRequest struct {
	Method string
	Url    url.URL
	Body   interface{}
	//Headers map[string]string
	//InTag   string
	//OutTag  string
	NotRefresh bool
}

type ResponseError struct {
	Error            string `json:"error"`
	ErrorDescription string `json:"error_description"`
}

func (h *HttpClient) request(optionsRequest *OptionsRequest, result interface{}) error {
	if !optionsRequest.NotRefresh && h.Token.ExpiresAt.Unix() < time.Now().Add(time.Minute).Unix() {
		_, err := h.refreshToken()
		if err != nil {
			return err
		}
	}

	body := &bytes.Buffer{}
	err := json.NewEncoder(body).Encode(optionsRequest.Body)
	if err != nil {
		return err
	}

	req, err := http.NewRequest(optionsRequest.Method, optionsRequest.Url.String(), bytes.NewReader(body.Bytes()))
	if err != nil {
		return err
	}

	defaultHeaders := map[string]string{
		"Accept":        "application/json",
		"Content-Type":  "application/json",
		"Cache-Control": "no-cache",
	}
	if h.Token.AccessToken != "" {
		defaultHeaders["Authorization"] = fmt.Sprintf("Bearer %s", h.Token.AccessToken)
	}

	for key, value := range defaultHeaders {
		req.Header.Set(key, value)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	data, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	fmt.Println(string(data))

	switch code := resp.StatusCode; true {
	case code == 200:
		var responseError ResponseError
		err = json.Unmarshal(data, &responseError)
		if err != nil {
			return err
		}
		if responseError.Error != "" {
			return errors.New(fmt.Sprintf("%s: %s", responseError.Error, responseError.ErrorDescription))
		}

		if result != nil {
			err = json.Unmarshal(data, &result)
			if err != nil {
				return err
			}
		}
	}
	return err
}

func (h *HttpClient) refreshToken() (*domain.Token, error) {
	option := OptionsRequest{
		Method: http.MethodPost,
		Url: url.URL{
			Scheme: "https",
			Host:   "api.avito.ru",
			Path:   refreshTokenResource,
			RawQuery: url.Values{
				"client_id":     []string{h.App.ClientId},
				"client_secret": []string{h.App.ClientSecret},
				"grant_type":    []string{"refresh_token"},
				"refresh_token": []string{h.Token.RefreshToken},
			}.Encode(),
		},
		NotRefresh: true,
	}

	now := time.Now()
	var token domain.Token
	err := h.request(&option, &token)
	if err != nil {
		return nil, err
	}

	token.ExpiresAt = now.Add(time.Duration(token.ExpiresIn) * time.Second)
	h.Token = &token

	if h.Callback != nil {
		h.Callback(h.Token)
	}

	return &token, nil
}
