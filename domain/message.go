package domain

type Message struct {
	Text string `json:"text"`
}

type SendMessage struct {
	Message Message `json:"message"`
	Type    string  `json:"type"`
}

type ResponseSendMessage struct {
	Contact struct {
		Test string `json:"test"`
	}
	Created   int64  `json:"created"`
	Direction string `json:"direction"`
	Id        string `json:"id"`
	Type      string `json:"type"`
}

type ResponseGetSubscriptions struct {
	Subscriptions []struct {
		Url     string `json:"url"`
		Version string `json:"version"`
	} `json:"subscriptions"`
}

type ResponseGetChatById struct {
	Id      string `json:"id"`
	Created int64  `json:"created"`
	Context struct {
		Type  string `json:"type"`
		Value struct {
			Id     int64 `json:"id"`
			Images struct {
				Count int64       `json:"count"`
				Main  interface{} `json:"main"`
			} `json:"images"`
			PriceString string `json:"price_string"`
			StatusId    int64  `json:"status_id"`
			Title       string `json:"title"`
			Url         string `json:"url"`
			UsedId      int64  `json:"used_id"`
		} `json:"value"`
	} `json:"context"`
	Users       Users
	LastMessage struct {
		AuthorId  int64  `json:"author_id"`
		Created   int64  `json:"created"`
		Direction string `json:"direction"`
		Id        string `json:"id"`
		Type      string `json:"type"`
	} `json:"last_message"`
}

type ResponseGetMessages struct {
	Messages []struct {
		AuthorId  int64  `json:"author_id"`
		Created   int64  `json:"created"`
		Direction string `json:"direction"`
		Id        string `json:"id"`
		IsRead    bool   `json:"is_read"`
		Read      int64  `json:"read"`
		Type      string `json:"type"`
		Content   struct {
			Call struct {
				Status       string `json:"status"`
				TargetUserId string `json:"target_user_id"`
			} `json:"call"`
			Image struct {
				Sizes map[string]string
			} `json:"image"`
			Item struct {
				PriceString string `json:"price_string"`
				Title       string `json:"title"`
				Url         string `json:"url"`
			} `json:"item"`
			Link struct {
				Text string `json:"text"`
				Url  string `json:"url"`
			} `json:"link"`
			Location struct {
				Kind  string  `json:"kind"`
				Lat   float64 `json:"lat"`
				Lon   float64 `json:"lon"`
				Text  string  `json:"text"`
				Title string  `json:"title"`
			} `json:"location"`
			Text string `json:"text"`
		} `json:"content"`
		Quote struct {
			AuthorId int64 `json:"author_id"`
			Content  struct {
				Call struct {
					Status       string `json:"status"`
					TargetUserId string `json:"target_user_id"`
				} `json:"call"`
				Image struct {
					Sizes map[string]string
				} `json:"image"`
				Item struct {
					PriceString string `json:"price_string"`
					Title       string `json:"title"`
					Url         string `json:"url"`
				} `json:"item"`
				Link struct {
					Text string `json:"text"`
					Url  string `json:"url"`
				} `json:"link"`
				Location struct {
					Kind  string  `json:"kind"`
					Lat   float64 `json:"lat"`
					Lon   float64 `json:"lon"`
					Text  string  `json:"text"`
					Title string  `json:"title"`
				} `json:"location"`
				Text string `json:"text"`
			} `json:"content"`
		}
	} `json:"messages"`
	Meta struct {
		HasMore bool `json:"has_more"`
	} `json:"meta"`
}

type Users []User

type User struct {
	Id                int64  `json:"id"`
	Name              string `json:"name"`
	PublicUserProfile struct {
		Avatar struct {
			Default string `json:"default"`
		}
		ItemId int64  `json:"item_id"`
		Url    string `json:"url"`
		UserId int64  `json:"user_id"`
	} `json:"public_user_profile"`
}

func (users *Users) GetById(id int64) *User {
	for _, user := range *users {
		if user.Id == id {
			return &user
		}
	}
	return nil
}

type WebhookSubscribe struct {
	Url string `json:"url"`
}

type ResponseWebhookSubscribe struct {
	Ok bool `json:"ok"`
}

type WebhookUnsubscribe struct {
	Url string `json:"url"`
}

type ResponseWebhookUnsubscribe struct {
	Ok bool `json:"ok"`
}

type HookMessage struct {
	Id        string `json:"id"`
	Version   string `json:"version"`
	Timestamp int64  `json:"timestamp"`
	Payload   struct {
		Type  string `json:"type"`
		Value struct {
			Id       string `json:"id"`
			ChatId   string `json:"chat_id"`
			UserId   int64  `json:"user_id"`
			AuthorId int64  `json:"author_id"`
			Created  int64  `json:"created"`
			Type     string `json:"type"`
			ChatType string `json:"chat_type"`
			Content  struct {
				Call struct {
					Status       string `json:"status"`
					TargetUserId string `json:"target_user_id"`
				} `json:"call"`
				Image struct {
					Sizes map[string]string
				} `json:"image"`
				Item struct {
					PriceString string `json:"price_string"`
					Title       string `json:"title"`
					Url         string `json:"url"`
				} `json:"item"`
				Link struct {
					Text string `json:"text"`
					Url  string `json:"url"`
				} `json:"link"`
				Location struct {
					Kind  string  `json:"kind"`
					Lat   float64 `json:"lat"`
					Lon   float64 `json:"lon"`
					Text  string  `json:"text"`
					Title string  `json:"title"`
				} `json:"location"`
				Text string `json:"text"`
			} `json:"content"`
			ItemId int64 `json:"item_id"`
		} `json:"value"`
	} `json:"payload"`
}
