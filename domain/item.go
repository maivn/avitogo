package domain

type ResponseGetItem struct {
	AutoloadItemId int64  `json:"autoload_item_id"`
	FinishTime     string `json:"finish_time"`
	StartTime      string `json:"start_time"`
	Status         string `json:"status"`
	Url            string `json:"url"`
}
