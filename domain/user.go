package domain

type ResponseGetUserInfoSelf struct {
	Id         int64  `json:"id"`
	Name       string `json:"name"`
	Phone      string `json:"phone"`
	Email      string `json:"email"`
	ProfileUrl string `json:"profile_url"`
}
